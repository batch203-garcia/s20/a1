let number;

numberPromt = prompt("Give me a number: ");
number = parseInt(numberPromt);

console.log("The number you provided is " + number);

for (let i = number; i > 0; i--) {
    if (i != 50) {
        if (i % 10 === 0) {
            console.log("current value is divisible by 10. Skipping the number")

        } else if (i % 5 === 0) {
            console.log(i)
            continue;
        }
    } else {
        console.log("The current value is at 50. Terminating the loop.")
        break;
    }
}

let string = "supercalifragilisticexpialidocious";
let consonants = ""

for (let i = 0; i < string.length; i++) {

    if (string[i].toLowerCase() == 'a' ||
        string[i].toLowerCase() == 'e' ||
        string[i].toLowerCase() == 'i' ||
        string[i].toLowerCase() == 'o' ||
        string[i].toLowerCase() == 'u') {
        consonants += "";
    } else {

        consonants += string[i];
    }
}


console.log(string);
console.log(consonants);